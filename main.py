#!/usr/bin/python3
import socket, os, subprocess, re, urllib.request, urllib.parse, textwrap

targets = {}
ports = {}

def initialise():	
		fp = open("ports.txt", "rt")
		for line in fp:
			line = line.replace('\n', '')
			l = re.split(",", line)
			ports[int(l[0])] = [l[1]]

def welcome():
	print("""This is the main menu. Choose a command:\n
 s   scan a new target
 v   view/investigate a scanned target
 f   filesystem scan
 d   database search
 x   quit\n""")

def dictCount(dictionary):
	count = 0
	for item in dictionary:
		count += 1
	return count

def scan():
	ip = "127.0.0.1"
	while True:
		ip = input("Enter the target address: ")
	
		targets[ip] = Target(ip)
		res = targets[ip].scanTarget()
	
		if res:
			print("Results found!")
		else:
			print("No results found.")
	
		while True:
			try:
				ans = input("Would you like to view the results (y/n)? ")
				ans = ans.lower()
				if ans == "y":
					searchVulns(targets[ip])
					return
				elif ans == "n":
					return
				else:
					raise TypeError
			except TypeError or EOFError:
				print("Please enter either (y)es or (n)o.")
				continue

def viewTargets():
	if not targets:
		print("No targets found.")
		return
	print("Here is a list of addresses that have been scanned. Choose one to interact with:\n")
	for target in targets:
		print(" ",target,":",dictCount(targets[target].openPorts),"open ports")
	print()
	while True:
		try:
			t = input("Target address: ")
			t = t.lower()
			if t == "x":
				return
			if t in targets:
				searchVulns(targets[t])
				return
			else:
				raise TypeError
		except TypeError or EOFError:
			print("Enter a valid target address.")
			continue

def searchVulns(target):
	target.showPorts()
	while True:
		try:
			port = input("Search for vulnerabilities on which port? ");
			if port == "x":
				return
			try:
				port = int(port) # creates error if number isn't given
			except:
				raise ValueError
			if port in target.openPorts:
				break
			else:
				raise TypeError
		except TypeError or EOFError or ValueError:
			print("ERR: Not a valid port")
			continue
				
	service = str(ports[port])
	service = service.strip("'[]'")
	print("Open port:",port)
	print("Service:",service)
	print("Raw message:",target.openPorts[port],'\n')
	print("Use the raw message to help answer the following queries.")
	databaseSearch()

def databaseSearch(software=None, version=None):
	if not software:
		while True:
			software = input("Name of service software (required): ")
			if not software:
				print("ERR: requires software name")
				continue
			break
	if not version:
		version = input("Version (recommended, use % as wildcard): ")
		if not version:
			version = "%"

	url = "https://www.cvedetails.com/version-search.php?vendor=&product={0}&version={1}".format(software, version)
	hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
		'Accept-Encoding': 'none',
		'Accept-Language': 'en-US,en;q=0.8',
		'Connection': 'keep-alive'}
	req = urllib.request.Request(url, headers=hdr)

	try:
		page = urllib.request.urlopen(req)
	except urllib.request.HTTPError as e:
		print(e.fp.read())

	content = page.read()
	paragraphs = re.findall(r'href="/vulnerability-list/(.*?)" title', str(content))

	vulnIDs = []
	for eachP in paragraphs:
		url = "https://www.cvedetails.com/vulnerability-list/{0}?order=3".format(eachP)
		req = urllib.request.Request(url, headers=hdr)
		try:
			page = urllib.request.urlopen(req)
		except urllib.request.HTTPError as e:
			print(e.fp.read())

		content = page.read()
		vulnIDs += re.findall(r'security vulnerability details">(.*?)</a>(?:.*?)<td>(?:.*?)</td>(?:.*?)<td>(?:.*?)</td>(?:.*?)<td>(.*?)?</td>(?:.*?)<td>(.*?)</td>(?:.*?)<div class="cvssbox"(?:.*?)>(.*?)</div>(?:.*?)class="cvesummarylong"(?:.*?)\\n\\t\\t\\t\\t\\t\\t(.*?)\\t\\t\\t\\t\\t</td>', str(content))

	vulnIDs = list(set(vulnIDs))
	vulnIDs.sort(key = lambda x: float(x[3]), reverse=True)
	print("\nCVE ID\t\tPublish Date\tUpdate Date\tScore\tDescription")
	print("-"*136)
	for cve,date1,date2,score,desc in vulnIDs:
		print("{}\t{}\t{}\t{}\t{}".format(cve,date1,date2,score,textwrap.fill(desc,width=80,subsequent_indent="\t\t\t\t\t\t\t")))
		print("-"*136)
	input("Press ENTER to continue...")

def fileScan():
	out = subprocess.run(['/bin/bash', '-c', 'compgen -c'], check=True, universal_newlines=True, stdout=subprocess.PIPE).stdout
	out = re.split('\n+', out.rstrip())
	for program in out:
		pass

class Target:
	buff = 1024
	def __init__(self, ip):
		self.ip = ip
		self.openPorts = {}

	def show(self):
		print("Target:", self.ip)

	def checkHost(self):
		if os.system("ping -c 1 " + self.ip + " > /dev/null 2>&1") == 0:
			return True
		else:
			return False

	def scanTarget(self):
		res = False # function returns false is no results
		if self.checkHost() is False:
			print("Host is down.")
			return res
		for port in range(1,65536):
			try:
				s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				s.connect((self.ip, port))
				s.settimeout(1)
			except socket.error as e:
				pass
			else:
				try:
					data = s.recv(self.buff)
				except socket.error as e:
					s.close()
				else:
					res = True
					print("Connection successful on port", port)
					self.openPorts[port] = [data]
					s.close()
		return res

	def showPorts(self):
		print("\nAnalysis results:\n")
		print("Port\tService\tRaw message")
		print("-"*96)
		for port in self.openPorts:
			print("{}\t{}\t{}".format(port,str(ports[port]).strip("'[]'"), textwrap.fill(str(self.openPorts[port]),width=80,subsequent_indent="\t\t")))
			print("-"*96)

initialise()

welcome()

mainFlag = 1
# Main menu
while(mainFlag):
	try:
		ans = input("> ")
		if ans == 's':
			scan()
		elif ans == 'v':
			viewTargets()
		elif ans == 'f':
			fileScan()
		elif ans == 'd':
			databaseSearch()
		elif ans == 'x':
			mainFlag = 0
			continue
		else:
			welcome()
	except:
		pass
print("Goodbye!")
